package org.mineacademy.orionbungee.command;

import java.util.List;

import org.mineacademy.bfo.annotation.AutoRegister;
import org.mineacademy.bfo.command.SimpleCommand;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@AutoRegister
public final class SwitchCommand extends SimpleCommand {

	public SwitchCommand() {
		super("switch");

		setMinArguments(1);
		setUsage("<serverName>");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final ProxiedPlayer player = getPlayer();
		final String serverName = args[0];

		final ProxyServer instance = ProxyServer.getInstance();
		final ServerInfo info = instance.getServerInfo(serverName);
		checkNotNull(info, "The server {0} does not exist! Available: " + String.join(", ", instance.getServers().keySet()));

		player.connect(info);
	}

	@Override
	protected List<String> tabComplete() {
		return args.length == 1 ? completeLastWordServerNames() : NO_COMPLETE;
	}
}
