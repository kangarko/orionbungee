package org.mineacademy.orionbungee.command;

import java.util.List;

import org.mineacademy.bfo.annotation.AutoRegister;
import org.mineacademy.bfo.command.SimpleCommand;
import org.mineacademy.bfo.model.SimpleScoreboard;
import org.mineacademy.bfo.remain.Remain;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

@AutoRegister
public final class FancyCommand extends SimpleCommand {

	public FancyCommand() {
		super("fancy");

		setMinArguments(1);
		setUsage("<title/action/tab/score>");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0].toLowerCase();
		final ProxiedPlayer player = getPlayer();
		final ProxyServer instance = ProxyServer.getInstance();

		if ("title".equals(param)) {
			for (final ServerInfo info : instance.getServers().values())
				for (final ProxiedPlayer onlinePlayer : info.getPlayers())
					Remain.sendTitle(onlinePlayer, "&4Welcome &cto &3" + onlinePlayer.getServer().getInfo().getName(), "&6Check &7out &four &dwebsite &9mineacademy.org");

			System.out.println("Sending title");
		}

		else if ("action".equals(param)) {
			Remain.sendActionBar(player, "&fWelcome to &b{current_server}");

			System.out.println("Sending action");

		} else if ("tab".equals(param)) {
			Remain.sendTablist(player, "\n&8[!] &6Welcome &8[!]\n", "\nCheck our site:\nmineacademy.org");

			System.out.println("Sending tab");

		} else if ("score".equals(param)) {
			final SimpleScoreboard scoreboard = new SimpleScoreboard(); // You can also externalize this to a field

			scoreboard.setTitle("&6OrionBungee");
			scoreboard.setLines("First line", "Second line", "", "Forth line", "Server: {current_server}");

			scoreboard.send(player);

			System.out.println("Sending score");
		}
	}

	@Override
	protected List<String> tabComplete() {
		return args.length == 1 ? completeLastWord("title", "action", "tab", "score") : NO_COMPLETE;
	}
}
