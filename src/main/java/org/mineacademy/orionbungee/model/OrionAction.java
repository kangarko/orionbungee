package org.mineacademy.orionbungee.model;

import org.mineacademy.bfo.bungee.BungeeMessageType;

import lombok.Getter;

public enum OrionAction implements BungeeMessageType {

	/**
	 * Represents a chat message.
	 *
	 * Data: the senders name, the message
	 */
	CHAT_MESSAGE(String.class, String.class);

	@Getter
	private final Class<?>[] content;

	OrionAction(final Class<?>... content) {
		this.content = content;
	}
}