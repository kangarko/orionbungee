package org.mineacademy.orionbungee;

import org.mineacademy.bfo.debug.Debugger;
import org.mineacademy.bfo.model.Variables;
import org.mineacademy.bfo.plugin.SimplePlugin;
import org.mineacademy.orionbungee.command.FancyCommand;
import org.mineacademy.orionbungee.command.SwitchCommand;
import org.mineacademy.orionbungee.listener.PlayerListener;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public final class OrionBungeePlugin extends SimplePlugin {

	@Override
	protected void onPluginStart() {
		Debugger.addDebuggedSection("bungee");

		registerCommand(new SwitchCommand());
		registerCommand(new FancyCommand());

		registerEvents(new PlayerListener());

		Variables.addVariable("current_server", sender -> sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getServer().getInfo().getName() : "");
	}
}
