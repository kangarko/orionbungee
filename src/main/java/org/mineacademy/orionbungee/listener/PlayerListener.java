package org.mineacademy.orionbungee.listener;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import org.mineacademy.bfo.Common;

public class PlayerListener implements Listener {

	@EventHandler
	public void onServerSwitch(final ServerSwitchEvent event) {
		final ProxiedPlayer player = event.getPlayer();
		final ServerInfo info = player.getServer().getInfo();

		Common.log("Player " + player.getName() + " switched to " + info.getName());
	}
}
