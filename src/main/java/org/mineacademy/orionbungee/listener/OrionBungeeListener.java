package org.mineacademy.orionbungee.listener;

import org.mineacademy.bfo.bungee.BungeeListener;
import org.mineacademy.bfo.bungee.message.IncomingMessage;
import org.mineacademy.orionbungee.model.OrionAction;

import lombok.Getter;
import net.md_5.bungee.api.connection.Connection;

public final class OrionBungeeListener extends BungeeListener {

	@Getter
	private final static OrionBungeeListener instance = new OrionBungeeListener();

	private OrionBungeeListener() {
		super("plugin:orion", OrionAction.class);
	}

	@Override
	public void onMessageReceived(final Connection serverConnection, final IncomingMessage message) {
		if (message.getAction() == OrionAction.CHAT_MESSAGE) {
			message.forwardToOthers();

			// This makes the message stay on BungeeCord however it sends it to every server including the sending one
			/*final String sender = message.readString();
			final String chatMessage = message.readString();

			ProxyServer.getInstance().broadcast(Common.toComponent(Common.colorize(sender + ": " + chatMessage)));*/
		}
	}
}
